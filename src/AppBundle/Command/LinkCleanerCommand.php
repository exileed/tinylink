<?php

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Класс команды для очистки ссылок с наступившим временем удаления
 *
 * Class LinkCleanerCommand
 * @package AppBundle\Command
 */
class LinkCleanerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('app:link_cleaner')
            ->setDescription('Delete links with expired time');
    }

    public function execute(InputInterface $input, OutputInterface $output)
    {
        $linkDeleter = $this->getContainer()->get('app.link_deleter');
        $linkDeleter->cleanOldLink();
    }
}
