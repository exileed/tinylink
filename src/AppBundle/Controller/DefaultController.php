<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Link;
use AppBundle\Services\LinkCreator;
use AppBundle\Services\RequestInfoExtractor;
use AppBundle\Services\VisitRegistrator;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    /**
     * @var LinkCreator
     */
    private $linkCreator;

    /**
     * @var VisitRegistrator
     */
    private $visitRegistrator;

    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * DefaultController constructor.
     * @param LinkCreator $linkCreator
     * @param VisitRegistrator $visitRegistrator
     * @param EntityManager $entityManager
     * @param Serializer $serializer
     */
    public function __construct(
        LinkCreator $linkCreator,
        VisitRegistrator $visitRegistrator,
        EntityManager $entityManager,
        Serializer $serializer
    ) {
        $this->linkCreator = $linkCreator;
        $this->visitRegistrator = $visitRegistrator;
        $this->entityManager = $entityManager;
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/create", name="createLink")
     */
    public function createTinylink(Request $request)
    {
        $originalLink = $request->get('link');
        if (empty($originalLink)) {
            throw  new NotFoundHttpException();
        }

        $expiredDate = $request->get('expiredDate');

        $link = $this->linkCreator->create($originalLink, $expiredDate);

        /**
         * @todo сделать сервис компоновщик ссылок, а может быть не надо
         */
        $response = new JsonResponse([
            'tinyLink'      => $request->getHost() . '/' . $link->getTiny(),
            'statisticLink' => $request->getHost() . '/statistic/' . $link->getTiny(),
        ]);
        $response->setEncodingOptions(JSON_UNESCAPED_SLASHES);

        return $response;
    }

    /**
     * @Route("/{tinyLink}", name="redirect")
     */
    public function redirectToFull(string $tinyLink, Request $request)
    {
        $link = $this->getLink($tinyLink);

        $this->visitRegistrator->register($link);

        return new RedirectResponse($link->getOriginal(), RedirectResponse::HTTP_MOVED_PERMANENTLY);
    }

    /**
     * @Route("/statistic/{tinyLink}", name="statistic")
     */
    public function getStatistic(string $tinyLink, Request $request)
    {

        $link = $this->getLink($tinyLink);

        /**
         * @todo какая то фигня получается
         */
        /** @var PersistentCollection $linkVisits */




        $linkVisits = $link->getVisits()->getValues();

        $newLinksVisits = [];
        foreach ($linkVisits as $visit) {
            $newLinksVisits[] = $this->serializer->serialize($visit, 'json');
        }


        $response = new JsonResponse([
            'linkVisits' => $newLinksVisits
        ]);
        $response->setEncodingOptions(JSON_UNESCAPED_SLASHES);

        return $response;
    }

    /**
     * Получаем ссылку по короткой иначе выбрасываем исключения(вынес в метод чтобы убить дубляж)
     *
     * @param $tinyLink
     * @return Link
     */
    private function getLink(string $tinyLink): Link
    {
        if (empty($tinyLink)) {
            throw  new NotFoundHttpException();
        }

        $link = $this->entityManager->find(Link::class, $tinyLink);

        if (empty($link)) {
            throw  new NotFoundHttpException();
        }

        return $link;
    }
}
