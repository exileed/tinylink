<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Класс обертка для получения разных данных из запроса
 *
 * Class RequestInfoExtractor
 * @package AppBundle\Services
 */
class RequestInfoExtractor
{
    /**
     * @var RequestStack;
     */
    private $requestStack;

    /**
     * @var Request
     */
    private $request;

    /**
     * RequestInfoExtractor constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->request = $this->requestStack->getCurrentRequest();
    }

    public function getGeo(): string
    {
        $geo = $this->request->getClientIp();

        /**
         * @todo прикрутить сервис получения geo инфы
         */
        $cities = ['Москваээ', 'ГусьХрустальныйэъ'];

        return $cities[rand(0,1)];
    }

    public function getUserAgent(): string
    {
        return $this->request->headers->get('User-Agent');
    }
}